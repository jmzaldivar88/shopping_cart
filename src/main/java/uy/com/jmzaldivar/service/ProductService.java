package uy.com.jmzaldivar.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import uy.com.jmzaldivar.domain.Product;
import uy.com.jmzaldivar.service.dto.ProductDTO;
import uy.com.jmzaldivar.service.dto.ProductoDescuentoDTO;
import uy.com.jmzaldivar.service.error.BusinessException;
import uy.com.jmzaldivar.service.implement.IProductService;
import uy.com.jmzaldivar.service.listeners.EventListenersProduct;
import uy.com.jmzaldivar.service.mapper.ProductMapper;
import uy.com.jmzaldivar.service.implement.IProductTransactional;
import uy.com.jmzaldivar.web.rest.dto.Result;
import uy.com.jmzaldivar.web.rest.utils.Variables;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Logger;

@Service
public class ProductService implements IProductService {
    private Logger logger = Logger.getLogger(ProductService.class.getName());
    private final IProductTransactional iProductTransactional;
    private final ProductMapper productMapper;
    private EventListenersProduct eventListenersProduct;

    public ProductService(IProductTransactional iProductTransactional,ProductMapper productMapper,EventListenersProduct eventListenersProduct) {
        this.iProductTransactional = iProductTransactional;
        this.productMapper = productMapper;
        this.eventListenersProduct = eventListenersProduct;
    }

    @Override
    public Result<ProductDTO>  create(ProductDTO productDto) {
        Result<ProductDTO> result = new Result<>();
        Product product1 = this.productMapper.toEntity(productDto);
        result.setData(this.productMapper.toDto(this.iProductTransactional.create(product1)));
        result.setStatus(HttpStatus.CREATED.value());

        return result;
    }

    @Override
    public Result<ProductDTO> modify(ProductDTO product) throws BusinessException {
        if(StringUtils.isBlank(product.getIdProduct()))
            throw new BusinessException(Variables.MSG_NOT_EXIST_ENTITY_IDENTITY);


        Result<ProductDTO> result = new Result<>();
        result.setStatus(HttpStatus.ACCEPTED.value());


        Product update = this.iProductTransactional.get(product.getIdProduct(),Boolean.TRUE).orElse(null);
        if (update == null) {
            throw new BusinessException(String.format(Variables.MSG_NOT_EXIST_ENTITY, Variables.ENTITY_NAME_PRODUCT, product.getIdProduct()));
        }

        update.setDescription((StringUtils.isNotBlank(product.getDescription())) ? product.getDescription() : update.getDescription());
        update.setName((StringUtils.isNotBlank(product.getName())) ? product.getName() : update.getName());
        update.setSku((StringUtils.isNotBlank(product.getSku())) ? product.getSku() : update.getSku());
        update.setTypeProduct(product.getTypeProduct() != null ? product.getTypeProduct() : update.getTypeProduct());
        update.setPrice(product.getPrice() != null ? product.getPrice() : update.getPrice());
        update.setDiscount(BigDecimal.ZERO);

        if (product instanceof ProductoDescuentoDTO) {
            update.setDiscount(new BigDecimal("0.5"));
        }
        result.setData(this.productMapper.toDto(this.iProductTransactional.modify(update)));
        return result;
    }

    @Override
    public Result<ProductDTO> get(String uuid) throws BusinessException {
        Result<ProductDTO> result = new Result<>();
        result.setStatus(HttpStatus.OK.value());
        result.setData(this.productMapper.toDto(this.iProductTransactional.get(uuid,Boolean.TRUE).orElseThrow( () -> new BusinessException(String.format(Variables.MSG_NOT_EXIST_ENTITY,Variables.ENTITY_NAME_PRODUCT)))));

        return result;
    }

    @Override
    public Result<Boolean> delete(String uuid) throws BusinessException {
        Result<Boolean> result = new Result<>();

            Product delete = this.iProductTransactional.get(uuid,Boolean.TRUE).orElseThrow(() -> new BusinessException(String.format(Variables.MSG_NOT_EXIST_ENTITY,Variables.ENTITY_NAME_PRODUCT)));
            delete.setActive(Boolean.FALSE);
            this.eventListenersProduct.deleteProductShoppingCart(delete);
            this.iProductTransactional.delete(delete);
            result.setStatus(HttpStatus.OK.value());
            result.setData(Boolean.TRUE);

        return result;
    }

    @Override
    public Result< List<ProductDTO>> listAll() {
        Result< List<ProductDTO>> result = new Result<>();

        List<ProductDTO> list = this.productMapper.toDto(this.iProductTransactional.getAll());
        list.forEach(productDTO -> {
            productDTO.setPrice(productDTO.priceFinal());
        });
        result.setData(list);
        result.setStatus(HttpStatus.OK.value());

        return result;
    }

    @Override
    public Result<ProductDTO> productById(String uuid) throws Exception {
        Result<ProductDTO> result = new Result<>();
        result.setData(this.productMapper.toDto(this.iProductTransactional.get(uuid,Boolean.TRUE).orElseThrow(() -> new Exception(String.format(Variables.MSG_NOT_EXIST_ENTITY,Variables.ENTITY_NAME_PRODUCT)))));
        result.setStatus(HttpStatus.OK.value());
        return result;
    }
}
