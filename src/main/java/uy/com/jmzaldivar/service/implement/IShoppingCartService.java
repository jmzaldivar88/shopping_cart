package uy.com.jmzaldivar.service.implement;

import org.springframework.stereotype.Service;
import uy.com.jmzaldivar.service.dto.ProductCartDTO;
import uy.com.jmzaldivar.service.dto.ShoppingCartDTO;
import uy.com.jmzaldivar.service.dto.TotalDto;
import uy.com.jmzaldivar.service.error.BusinessException;
import uy.com.jmzaldivar.web.rest.dto.Result;

import java.util.List;
@Service
public interface IShoppingCartService {

    Result<Boolean> create(ShoppingCartDTO cartDTO) throws BusinessException;
    Result<Boolean> modify(ShoppingCartDTO cartDTO) throws BusinessException;
    Result<Boolean> delete(String uuidProduct);
    Result<List<ProductCartDTO>> listProductByCarrito(Boolean checkout);
    Result<TotalDto> checkout() throws BusinessException;
}
