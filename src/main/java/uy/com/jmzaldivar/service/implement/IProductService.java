package uy.com.jmzaldivar.service.implement;

import org.springframework.stereotype.Service;
import uy.com.jmzaldivar.service.dto.ProductDTO;
import uy.com.jmzaldivar.service.error.BusinessException;
import uy.com.jmzaldivar.web.rest.dto.Result;

import java.util.List;

@Service
public interface IProductService {

    Result<ProductDTO> create(ProductDTO product) ;
    Result<ProductDTO> modify(ProductDTO product) throws BusinessException;
    Result<ProductDTO> get(String uuid) throws BusinessException;
    Result<Boolean> delete(String uuid) throws BusinessException;
    Result<List<ProductDTO>> listAll();
    Result<ProductDTO> productById(String uuid) throws Exception;
}
