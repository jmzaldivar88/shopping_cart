package uy.com.jmzaldivar.service.implement;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;
@Service
public interface IShoppingCart {

    BigDecimal total();
}
