package uy.com.jmzaldivar.service.implement;

import org.springframework.stereotype.Service;
import uy.com.jmzaldivar.domain.Product;
import uy.com.jmzaldivar.service.error.BusinessException;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Service
public interface IProductTransactional {
    Product create(Product product) ;
    Product modify(Product product);
    Optional<Product> get(String uuid,Boolean active);
    Optional<Product> getBySku(String sku, Boolean active);
    List<Product> getAll(List<String> uuid,Boolean active);
    void delete(Product product) ;
    List<Product> getAll();
}
