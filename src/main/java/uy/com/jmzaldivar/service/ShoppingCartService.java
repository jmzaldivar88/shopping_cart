package uy.com.jmzaldivar.service;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import uy.com.jmzaldivar.domain.Product;
import uy.com.jmzaldivar.domain.ShoppingCart;
import uy.com.jmzaldivar.service.dto.*;
import uy.com.jmzaldivar.service.enumerator.StateCart;
import uy.com.jmzaldivar.service.error.BusinessException;
import uy.com.jmzaldivar.service.implement.IShoppingCartService;
import uy.com.jmzaldivar.service.mapper.CartMapper;
import uy.com.jmzaldivar.service.mapper.ProductMapper;
import uy.com.jmzaldivar.service.transactional.CartTransactional;
import uy.com.jmzaldivar.service.transactional.ProductTransactional;
import uy.com.jmzaldivar.web.rest.dto.Result;
import uy.com.jmzaldivar.web.rest.utils.SecurityUtils;
import uy.com.jmzaldivar.web.rest.utils.Variables;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Service
public class ShoppingCartService implements IShoppingCartService {

    private final CartTransactional cartTransactional;
    private final ProductTransactional productTransactional;
    private final CartMapper cartMapper;
    private final ProductMapper productMapper;


    public ShoppingCartService(CartTransactional cartTransactional,ProductTransactional productTransactional, CartMapper cartMapper,ProductMapper productMapper) {
        this.cartTransactional = cartTransactional;
        this.productTransactional = productTransactional;
        this.cartMapper = cartMapper;
        this.productMapper =productMapper;
    }

    @Override
    public Result<Boolean> create(ShoppingCartDTO cartDTO) throws BusinessException {
        Result<Boolean> result = new Result<>();

        String username = SecurityUtils.USER_AUTENTICATE;
        CartDTO create = new CartDTO();
        create.setState(StateCart.PENDIENTE.getValue());
        create.setProducto(new ProductDTO().idProduct(cartDTO.getIdProducto()));
        create.setAmount(cartDTO.getAmount());
        create.setUsername(username);
        this.cartTransactional.create(this.cartMapper.toEntity(create));
        result.setData(Boolean.TRUE);
        result.setStatus(HttpStatus.CREATED.value());

        return result;
    }

    @Override
    public Result<Boolean> modify(ShoppingCartDTO cartDTO) throws BusinessException {
        Result<Boolean> result = new Result<>();

        String username = SecurityUtils.USER_AUTENTICATE;
       ShoppingCart cart = this.cartTransactional.get(
                username,
                this.productTransactional.get(cartDTO.getIdProducto(),Boolean.TRUE).orElseThrow(()->new BusinessException(String.format(Variables.MSG_NOT_EXIST_PRODUCT_CART,Variables.ENTITY_NAME_PRODUCT))
                )).orElseThrow(()->new BusinessException(String.format(Variables.MSG_NOT_EXIST_PRODUCT_CART,Variables.ENTITY_NAME_PRODUCT)));

        cart.setCantidad(cartDTO.getAmount());
        this.cartTransactional.modify(cart);
        result.setData(Boolean.TRUE);
        result.setStatus(HttpStatus.ACCEPTED.value());

        return result;
    }

    @Override
    public Result<Boolean> delete(String uuidProduct) {
        Result<Boolean> result = new Result<>();
        String username = SecurityUtils.USER_AUTENTICATE;
        this.cartTransactional.deleteProductCart(username,this.productTransactional.get(uuidProduct,Boolean.TRUE).orElse(new Product()));
        result.setStatus(HttpStatus.OK.value());
        result.setData(Boolean.TRUE);
        return result;
    }

    @Override
    public Result<List<ProductCartDTO>> listProductByCarrito(Boolean checkout) {
        Result<List<ProductCartDTO>>  result = new Result<>();

            String username = SecurityUtils.USER_AUTENTICATE;
            List<ShoppingCart> modifyCart = new ArrayList<>();
            List<ProductCartDTO> list = this.cartTransactional.listProductos(username).stream().peek(shoppingCart -> {
                if(checkout){
                    shoppingCart.setStateCart(StateCart.COMPLETADO);
                    modifyCart.add(shoppingCart);
                }
            }).map(shoppingCart -> new ProductCartDTO().amount(shoppingCart.getCantidad()).productDTO(this.productMapper.toDto(shoppingCart.getProduct()))).collect(Collectors.toList());
                list.forEach(productDTO -> {
                    if (!checkout)
                        productDTO.getProductDTO().setPrice(productDTO.getProductDTO().priceFinal());
            });

            if(checkout){
                this.cartTransactional.modifyAll(modifyCart);
            }

            result.setStatus(HttpStatus.OK.value());
            result.setData(list);

        return result;
    }

    @Override
    public Result<TotalDto> checkout() throws BusinessException {
        Result<TotalDto> result = new Result<>();
        result.setStatus(HttpStatus.OK.value());

            Result<List<ProductCartDTO>> listcarrito = this.listProductByCarrito(Boolean.TRUE);

            if(listcarrito.getStatus() == HttpStatus.OK.value()){
                List<ProductCartDTO> productCartDTOS = listcarrito.getData();
                AtomicReference<BigDecimal> sum = new AtomicReference<>(BigDecimal.ZERO);
                productCartDTOS.forEach(cartDTO -> {
                    sum.set(sum.get().add(cartDTO.total()));
                });
                TotalDto totalDto = new TotalDto();
                totalDto.setTotal(sum.get().setScale(2, RoundingMode.HALF_UP));
                result.setData(totalDto);
            }else{
                throw new BusinessException(listcarrito.getError());
            }

       return  result;
    }
}
