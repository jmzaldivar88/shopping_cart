package uy.com.jmzaldivar.service.transactional;

import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import uy.com.jmzaldivar.domain.Product;
import uy.com.jmzaldivar.domain.ShoppingCart;
import uy.com.jmzaldivar.repository.ShoppingCartRepository;
import uy.com.jmzaldivar.service.enumerator.StateCart;
import uy.com.jmzaldivar.service.implement.IShoppingCartTransactional;

import java.util.List;
import java.util.Optional;

@Service
public class CartTransactional implements IShoppingCartTransactional {
    private final ShoppingCartRepository shoppingCartRepository;

    public CartTransactional(ShoppingCartRepository shoppingCartRepository) {
        this.shoppingCartRepository = shoppingCartRepository;
    }

    @Override
    public ShoppingCart create(ShoppingCart shoppingCart) {
        return this.shoppingCartRepository.save(shoppingCart);
    }

    @Override
    public List<ShoppingCart> createAll(List<ShoppingCart> shoppingCartList) {
        return this.shoppingCartRepository.saveAll(shoppingCartList);
    }

    @Override
    public ShoppingCart modify(ShoppingCart shoppingCart) {
        return this.shoppingCartRepository.save(shoppingCart);
    }

    @Override
    public List<ShoppingCart> modifyAll(List<ShoppingCart> shoppingCarts) {
        return this.shoppingCartRepository.saveAll(shoppingCarts);
    }

    @Override
    public Optional<ShoppingCart> get(String username, Product product) {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setUsuario(username);
        shoppingCart.setProduct(product);
        shoppingCart.setStateCart(StateCart.PENDIENTE);
        Example<ShoppingCart> example = Example.of(shoppingCart);
        return this.shoppingCartRepository.findOne(example);
    }

    @Override
    public void delete(String username) {
        this.shoppingCartRepository.deleteAll(this.listProductos(username));
    }

    @Override
    public void deleteProductCart(String username, Product product) {
        ShoppingCart shoppingCart = this.shoppingCartRepository.findByUsuarioAndProductAndActiveAndStateCart(username,product,Boolean.TRUE,StateCart.PENDIENTE).orElse(null);
        if(shoppingCart != null) {
            shoppingCart.setActive(Boolean.FALSE);
            this.shoppingCartRepository.save(shoppingCart);
        }
    }


    @Override
    public List<ShoppingCart> listProductos(String username) {

        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setUsuario(username);
        shoppingCart.setStateCart(StateCart.PENDIENTE);
        Example<ShoppingCart> example = Example.of(shoppingCart);

        return this.shoppingCartRepository.findAll(example);
    }

}
