package uy.com.jmzaldivar.service.transactional;

import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import uy.com.jmzaldivar.domain.Product;
import uy.com.jmzaldivar.repository.ProductRepository;
import uy.com.jmzaldivar.service.implement.IProductTransactional;
import java.util.List;
import java.util.Optional;

@Service
public class ProductTransactional implements IProductTransactional {
    private final ProductRepository productRepository;

    public ProductTransactional(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Product create(Product product)     {
        return this.productRepository.save(product);
    }

    @Override
    public Product modify(Product product) {

        return this.productRepository.save(product);
    }

    @Override
    public Optional<Product> get(String uuid,Boolean active)     {
        return this.productRepository.findByIdProductAndActive(uuid,active);
    }

    @Override
    public Optional<Product> getBySku(String sku,Boolean active)      {
        return this.productRepository.findBySkuAndActive(sku,active);
    }

    @Override
    public List<Product> getAll(List<String> uuids,Boolean active)      {
        return this.productRepository.findAllByIdProductInAndActive(uuids,active);
    }

    @Override
    public void delete(Product product)   {
        this.productRepository.saveAndFlush(product);
    }

    @Override
    public List<Product> getAll()  {
        Product product = new Product();
        product.setActive(Boolean.TRUE);
        Example<Product> example = Example.of(product);
        return this.productRepository.findAll(example);
    }


}
