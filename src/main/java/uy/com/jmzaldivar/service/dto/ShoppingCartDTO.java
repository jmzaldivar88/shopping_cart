package uy.com.jmzaldivar.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ShoppingCartDTO {

    @ApiModelProperty(required = true, value = "")
    @JsonProperty(value = "amount")
    @NotNull
    private Integer amount;

    @ApiModelProperty(required = true, value = "")
    @JsonProperty(value = "idProduct")
    @NotBlank
    private String idProducto;



    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }
}
