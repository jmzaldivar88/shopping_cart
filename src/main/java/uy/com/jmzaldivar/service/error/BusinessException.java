package uy.com.jmzaldivar.service.error;

public class BusinessException extends Throwable{

    private String message;
    public BusinessException(String s) {
       this.message = s;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
