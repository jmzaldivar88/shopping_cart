package uy.com.jmzaldivar.service.listeners;

import org.springframework.stereotype.Service;
import uy.com.jmzaldivar.domain.Product;
import uy.com.jmzaldivar.service.transactional.CartTransactional;
import uy.com.jmzaldivar.web.rest.utils.SecurityUtils;

@Service
public class EventListenersProduct {
    private final CartTransactional cartTransactional;

    public EventListenersProduct(CartTransactional cartTransactional) {
        this.cartTransactional = cartTransactional;
    }

    public void deleteProductShoppingCart(Product product){
        if(!product.getActive()){
            this.cartTransactional.deleteProductCart(SecurityUtils.USER_AUTENTICATE,product);
        }
    }
}
