package uy.com.jmzaldivar.service.mapper;

import uy.com.jmzaldivar.service.error.BusinessException;

import java.util.List;

/**
 * Contract for a generic dto to entity mapper.
 *
 * @param <D> - DTO type parameter.
 * @param <E> - Entity type parameter.
 */

public interface EntityMapper<D, E> {

    E toEntity(D dto) throws BusinessException;

    D toDto(E entity) throws BusinessException;

    List <E> toEntity(List<D> dtoList) throws BusinessException;

    List <D> toDto(List<E> entityList) throws BusinessException;
}
