package uy.com.jmzaldivar.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uy.com.jmzaldivar.domain.Product;
import uy.com.jmzaldivar.domain.ShoppingCart;
import uy.com.jmzaldivar.service.enumerator.StateCart;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ShoppingCartRepository extends JpaRepository<ShoppingCart, UUID> {

    Optional<ShoppingCart> findByUsuarioAndProductAndActiveAndStateCart(String user, Product product, Boolean active, StateCart stateCart);
}
