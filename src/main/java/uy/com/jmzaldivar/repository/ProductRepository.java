package uy.com.jmzaldivar.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uy.com.jmzaldivar.domain.Product;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, String> {
    Optional<Product> findByIdProductAndActive(String uuid, Boolean aTrue);

    List<Product> findAllByIdProductInAndActive(List<String> uuids, Boolean aTrue);

    Optional<Product> findBySkuAndActive(String sku, Boolean active);
}
