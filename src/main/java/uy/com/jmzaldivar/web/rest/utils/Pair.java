package uy.com.jmzaldivar.web.rest.utils;

import io.micrometer.core.lang.NonNull;

import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public final class Pair<S, T> {
    @NonNull
    private final S first;

    private final T second;

    public static <S, T> Pair<S, T> of(S first, T second) {
        return new Pair(first, second);
    }
    public static <S, T> Collector<Pair<S, T>, ?, Map<S, T>> toMap() {
        return Collectors.toMap(Pair::getFirst, Pair::getSecond);
    }

    public S getFirst() {
        return this.first;
    }

    public T getSecond() {
        return this.second;
    }

    private Pair(@NonNull S first,  T second) {
        if (first == null) {
            throw new IllegalArgumentException("first is marked non-null but is null");
        } else {
            this.first = first;
            this.second = second;
        }
    }


}
