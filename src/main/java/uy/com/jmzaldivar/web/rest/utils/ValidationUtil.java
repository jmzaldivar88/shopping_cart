package uy.com.jmzaldivar.web.rest.utils;



import uy.com.jmzaldivar.service.error.BusinessException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class ValidationUtil {

    public static void validParams(List<Pair<String, Object>> params) throws BusinessException {
        Map<String, Object> tmp = new HashMap<>();
        for (int i = 0; i < params.size(); i++) {
            if (params.get(i).getSecond() == null || params.get(i).getSecond() instanceof List && ((List) params.get(i).getSecond()).size() == 0) {
                tmp.put("param" + i, params.get(i).getFirst());
            }
        }
        if (tmp.size() > 0) throw new BusinessException("Los campos no pueden ser nulo o vacios: [ "+tmp.values().stream().map(o -> "'"+ o.toString()+"'".concat(" ")).collect(Collectors.joining())+"]");

    }
}
