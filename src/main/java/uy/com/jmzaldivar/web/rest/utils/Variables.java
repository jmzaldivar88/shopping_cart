package uy.com.jmzaldivar.web.rest.utils;

public class Variables {
    public static final String APP_NAME = "Carrito";
    public static final String ENTITY_NAME_PRODUCT = "Producto";
    public static final String ENTITY_NAME_SHOPPING_CART = "Carrito de compra";
    public  static final String MSG_NOT_EXIST_ENTITY = "No existe el %s";
    public  static final String MSG_NOT_EXIST_ENTITY_IDENTITY = "EL identificador del producto es vacío.";
    public  static final String MSG_NOT_EXIST_PRODUCT_CART= "No existe el %s en el carrito.";

    public  static final String MSG_NOT_CONSTRAINT_ENTITY = "El sku del producto debe ser único";

}
