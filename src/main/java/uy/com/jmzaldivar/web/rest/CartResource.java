package uy.com.jmzaldivar.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uy.com.jmzaldivar.service.dto.ProductCartDTO;
import uy.com.jmzaldivar.service.dto.ShoppingCartDTO;
import uy.com.jmzaldivar.service.dto.TotalDto;
import uy.com.jmzaldivar.service.error.BusinessException;
import uy.com.jmzaldivar.service.implement.IShoppingCartService;
import uy.com.jmzaldivar.web.rest.dto.Result;
import uy.com.jmzaldivar.web.rest.implement.IShoppingCartResouce;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class CartResource implements IShoppingCartResouce {

    @Autowired
    private IShoppingCartService iShoppingCartService;

    @Override
    public Result<List<ProductCartDTO>> getAllProductByCarrito() {
        return this.iShoppingCartService.listProductByCarrito(Boolean.FALSE);
    }

    @Override
    public Result<TotalDto> checkout() throws BusinessException {
        return this.iShoppingCartService.checkout();
    }

    @Override
    public Result<Boolean> addProductCart(ShoppingCartDTO cartDTO) throws BusinessException {
        return this.iShoppingCartService.create(cartDTO);
    }

    @Override
    public Result<Boolean> modifyProductCart(ShoppingCartDTO cartDTO) throws BusinessException {

        return this.iShoppingCartService.modify(cartDTO);
    }

    @Override
    public Result<Boolean> deleteProduct(String uuidProduct)  {
        return this.iShoppingCartService.delete(uuidProduct);
    }
}
