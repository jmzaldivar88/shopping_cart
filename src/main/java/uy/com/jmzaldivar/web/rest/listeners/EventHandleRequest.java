package uy.com.jmzaldivar.web.rest.listeners;

import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import uy.com.jmzaldivar.service.ProductService;
import uy.com.jmzaldivar.service.error.BusinessException;
import uy.com.jmzaldivar.web.rest.dto.Result;


import javax.validation.UnexpectedTypeException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@ControllerAdvice
public class EventHandleRequest {

    private Logger logger = Logger.getLogger(ProductService.class.getName());

   @ResponseBody
    @ExceptionHandler(UnexpectedTypeException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    Result<Boolean> response(UnexpectedTypeException ex) {
        Result<Boolean> result = new Result<>();
        result.setData(false);
        result.setStatus(HttpStatus.BAD_REQUEST.value());
        result.setError(ex.getMessage());
        return result;
    }

    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    Result<Boolean> response(MethodArgumentNotValidException ex) {
       /* Map<String,FieldError> map = new HashMap<>();
        ex.getBindingResult().getFieldErrors().stream().map(fieldError -> map.put(fieldError.getField(),fieldError));*/
        Result<Boolean> result = new Result<>();
        result.setData(false);
        result.setStatus(HttpStatus.BAD_REQUEST.value());
        result.setError(ex.getMessage());
    return result;
    }



    @ResponseBody
    @ExceptionHandler(BusinessException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    Result<Boolean> response(BusinessException ex) {
        Result<Boolean> result = new Result<>();
        result.setData(false);
        result.setStatus(HttpStatus.BAD_REQUEST.value());
        result.setError(ex.getMessage());
        return result;
    }

    @ResponseBody
    @ExceptionHandler(SQLException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    Result<Boolean> response(SQLException ex) {
        Result<Boolean> result = new Result<>();
        result.setData(false);
        result.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        result.setError(ex.getMessage());
        return result;
    }

    @ResponseBody
    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    Result<Boolean> response(HttpMessageNotReadableException ex) {
        Result<Boolean> result = new Result<>();
        result.setData(false);
        result.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        result.setError(ex.getMessage());
        return result;
    }




}
