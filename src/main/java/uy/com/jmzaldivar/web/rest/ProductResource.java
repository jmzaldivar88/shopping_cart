package uy.com.jmzaldivar.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uy.com.jmzaldivar.service.dto.ProductDTO;
import uy.com.jmzaldivar.service.error.BusinessException;
import uy.com.jmzaldivar.service.implement.IProductService;
import uy.com.jmzaldivar.web.rest.dto.Result;
import uy.com.jmzaldivar.web.rest.implement.IProductResource;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class ProductResource implements IProductResource {



    @Autowired
    private IProductService iProductService;

    @Override
    public Result<ProductDTO> createProduct(ProductDTO product) {
        return  this.iProductService.create(product);
    }

    @Override
    public Result<ProductDTO> updateProduct(ProductDTO product) throws BusinessException {
        return this.iProductService.modify(product);
    }

    @Override
    public Result<Boolean> deleteProduct(String uuid) throws BusinessException {
        return this.iProductService.delete(uuid);
    }

    @Override
    public Result<List<ProductDTO>> listProduct() {
        Result<List<ProductDTO>>  result = this.iProductService.listAll();
        return result;
    }
}
