package uy.com.jmzaldivar.domain;

import org.hibernate.annotations.GenericGenerator;
import uy.com.jmzaldivar.service.enumerator.StateCart;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "T_SHOPPING_CART")
public class ShoppingCart implements Serializable {
    @Id
    @GenericGenerator(name="shoppingCartGenerator", strategy="uuid")
    @GeneratedValue(generator="shoppingCartGenerator")
    @Column(name = "ID")
    private String id;

    @Column(name = "usuario")
    private String usuario;

    @Column(name = "STATE_CART")
    @Enumerated(EnumType.STRING)
    private StateCart stateCart;

    @Column(name = "AMOUNT")
    private Integer cantidad;


    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    @Column(name = "active")
    private Boolean active = Boolean.TRUE;

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public StateCart getStateCart() {
        return stateCart;
    }

    public void setStateCart(StateCart stateCart) {
        this.stateCart = stateCart;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
