package uy.com.jmzaldivar.domain;

import org.hibernate.annotations.GenericGenerator;
import uy.com.jmzaldivar.service.enumerator.TypeProduct;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "T_PRODUCT")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
public class Product implements Serializable {

    @Id
    @GenericGenerator(name="productGenerator", strategy="uuid")
    @GeneratedValue(generator="productGenerator")
    @Column(name = "ID")
    private String idProduct;

    @Column(name = "NAME")
    @NotBlank(message = "El nombre del producto es obligatorio.")
    private String name;

    @Column(name = "SKU", unique = true)
    private String sku;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "PRICE" ,scale = 2)
    private BigDecimal price;

    @Column(name = "TYPE_PRODUCT")
    @Enumerated(EnumType.STRING)
    private TypeProduct typeProduct;

    @Column(name = "discount")
    private BigDecimal discount = new BigDecimal("0.5");

    @Column(name = "active")
    private Boolean active = Boolean.TRUE;


    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public TypeProduct getTypeProduct() {
        return typeProduct;
    }

    public void setTypeProduct(TypeProduct typeProduct) {
        this.typeProduct = typeProduct;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    @Override
    public String toString() {
        return "PRODUCT";
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
